#include "equalizerbar.h"
#include "ui_mainwindow.h"

EqualizerBar::EqualizerBar(QWidget *parent) :
    QWidget(parent)
{
    this->setSizePolicy(
        QSizePolicy::MinimumExpanding,
        QSizePolicy::MinimumExpanding
    );
    for(unsigned int i = 0; i < _nbBars; i++) {
        _values.push_back(0);
    }
    for(unsigned int i = 0; i < _nbSteps; i++) {
        _colors.push_back(QColor("black"));
    }
}

EqualizerBar::~EqualizerBar() {}

void EqualizerBar::paintEvent(QPaintEvent *event) {
    QPainter painter(this);

    QBrush brush = *new QBrush();
    brush.setColor(_backgroundColor);
    brush.setStyle(Qt::SolidPattern);
    QRect rect(0, 0, this->width(), this->height());
    painter.fillRect(rect, brush);

    int d_height = this->height() - (_padding * 2);
    int d_width = this->width() - (_padding * 2);

    float step_y = float(d_height) / _nbSteps;
    float bar_height = step_y * _ySolidPercent;
    float bar_height_space = step_y * (1 - _xSolidPercent) /2;

    float step_x = float(d_width) / _nbBars;
    float bar_width = step_x * _xSolidPercent;
    float bar_width_space = step_x * (1 - _ySolidPercent) /2;

    for(unsigned int i = 0; i < _nbBars; i++) {
        float pc = (_values.at(i) - _vmin) / (_vmax - _vmin);
        unsigned int n_steps_to_draw = pc * _nbSteps;

        for(unsigned int j = 0; j < n_steps_to_draw; j++) {
            brush.setColor(QColor(_colors[j]));
            QRect rect(int(_padding + (step_x * i) + bar_width_space), int(_padding + d_height - ((1 + j) * step_y) + bar_height_space), int(bar_width), int(bar_height));
            painter.fillRect(rect, brush);
        }
    }
    painter.end();
}

void EqualizerBar::triggerRefresh() {
    update();
}

std::vector<float> EqualizerBar::getValues() {
    return _values;
}

void EqualizerBar::setValues(std::vector<float> v) {
    _nbBars = v.size();
    _values = v;
    update();
}

void EqualizerBar::setRange(float vmin, float vmax) {
    _vmax = std::max(vmin, vmax);
    _vmin = std::min(vmin, vmax);
}

void EqualizerBar::setColor(QColor color) {
    for(unsigned int i = 0; i < _nbBars; i++) {
        _colors.at(i) = color;
    }
}

void EqualizerBar::setColors(std::vector<QColor> colors) {
    _nbSteps = colors.size();
    _colors = colors;
    update();
}

void EqualizerBar::setHslColors(QColor c, unsigned int nbSteps) {
    std::vector<QColor> colors;
    for(int i = 0; i < int(nbSteps); i++) {
        colors.push_back(QColor::fromHsl(c.hue(),c.saturation(),int(255 * i / int(nbSteps))));
    }
    setColors(colors);
}

void EqualizerBar::setGradient(QColor c1, QColor c2, unsigned int nbSteps) {
    std::vector<QColor> colors;
    for(int i = 0; i < int(nbSteps); i++) {
        int r = c1.red() + (- c1.red() + c2.red()) * (float(i) / int(nbSteps));
        int g = c1.green() + (- c1.green() + c2.green()) * (float(i) / int(nbSteps));;
        int b = c1.blue() + (- c1.blue() + c2.blue()) * (float(i) / int(nbSteps));;
        colors.push_back(QColor(r, g, b));
    }
    setColors(colors);
}

void EqualizerBar::setBarPadding(int i) {
    _padding = i;
    update();
}

void EqualizerBar::setBackgroundColor(QColor color) {
    _backgroundColor = color;
    update();
}
