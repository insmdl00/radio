#ifndef EQUALIZERBAR_H
#define EQUALIZERBAR_H

#include <QWidget>
#include <QPainter>
#include <QTimer>
#include <iostream>

class EqualizerBar : public QWidget
{
    Q_OBJECT

public:
    EqualizerBar(QWidget *parent = nullptr);
    ~EqualizerBar();

    virtual void paintEvent(QPaintEvent *event);
    void triggerRefresh();
    void decayBeat();

    std::vector<float> getValues();

    void setValues(std::vector<float> v);
    void setRange(float vmin, float vmax);
    void setColor(QColor color);
    void setColors(std::vector<QColor> colors);
    void setHslColors(QColor c, unsigned int nbSteps);
    void setGradient(QColor c1, QColor c2, unsigned int nbSteps);
    void setBarPadding(int i);
    void setBackgroundColor(QColor color);

private:
    unsigned int _nbBars = 10;
    unsigned int _nbSteps = 10;
    std::vector<QColor> _colors;
    QColor _backgroundColor = "white";

    float _xSolidPercent = float(0.85);
    float _ySolidPercent = float(0.9);
    int _padding = 10;

    float _vmin = 0;
    float _vmax = 100;
    std::vector<float> _values;
};

#endif // EQUALIZERBAR_H
