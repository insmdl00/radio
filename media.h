#ifndef MEDIA_H
#define MEDIA_H

#include <QString>
#include <QUrl>
#include <QColor>
#include <QIcon>
#include <QMediaPlayer>
#include <QMediaContent>
#include <string>
#include <vector>

// Class Media
class Media {
public:
    Media(QMediaContent media, QString url, QString titre, QIcon icon);
    ~Media() {}

    // Fonction set change les valeurs d'un media
    void setTitre(QString titre);
    void setUrl(QString url);
    void setFavori(bool val);

    // Fonction get qui renvois les valeurs d'un media
    QMediaContent getMedia();
    std::string toStringTitre();
    std::string toStringUrl();
    QString getTitre();
    QString getUrl();
    QIcon getIcon();
    bool getFavori();
    bool valideMedia();

private:
    QMediaContent _media;
    QIcon _icon;
    QString _url;
    QString _titre;
    bool _favori;
};

// Class LocalMedia
class LocalMedia : public Media {
public:
    LocalMedia(QString url, QString titre, QIcon icon);
    ~LocalMedia() {}
};

// Class OnlineMedia
class OnlineMedia : public Media {
public:
    OnlineMedia(QString url, QString titre, QIcon icon);
    ~OnlineMedia() {}
};

#endif // MEDIA_H
