#include <media.h>

// Class Radio
Media::Media(QMediaContent media, QString url, QString titre, QIcon icon): _media(media), _url(url), _titre(titre), _icon(icon), _favori(false) {}

// Fonction set change les valeurs d'un media
void Media::setTitre(QString titre) {
    _titre = titre;
}
void Media::setUrl(QString url) {
    _url = url;
}
void Media::setFavori(bool val) {
    _favori = val;
}

// Fonction qui renvois les valeurs d'un media
QMediaContent Media::getMedia() {
    return _media;
}
std::string Media::toStringTitre() {
    return _titre.toStdString();
}
std::string Media::toStringUrl() {
    return _url.toStdString();
}
QString Media::getTitre() {
    return _titre;
}
QString Media::getUrl() {
    return _url;
}
QIcon Media::getIcon() {
    return _icon;
}
bool Media::getFavori() {
    return _favori;
}

bool Media::valideMedia() {
    //test url valide ou non et l'audio de l'url
    QMediaPlayer itPlayer;
    itPlayer.setMedia(_media);
    itPlayer.play();
    return(itPlayer.state());
}

// Class LocalMedia
LocalMedia::LocalMedia(QString url, QString titre, QIcon icon): Media(QMediaContent(QUrl::fromLocalFile(url)), url, titre, icon) {}

// Class OnlineMedia
OnlineMedia::OnlineMedia(QString url, QString titre, QIcon icon): Media(QMediaContent(QUrl(url, QUrl::StrictMode)), url, titre, icon) {}
