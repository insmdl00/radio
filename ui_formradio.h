/********************************************************************************
** Form generated from reading UI file 'formradio.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMRADIO_H
#define UI_FORMRADIO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FormMedia
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *addRadio;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_2;
    QLineEdit *editAddTitre;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *editUrl;
    QWidget *removeRadio;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *editRemoveTitre;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *FormMedia)
    {
        if (FormMedia->objectName().isEmpty())
            FormMedia->setObjectName(QString::fromUtf8("FormMedia"));
        FormMedia->resize(600, 120);
        FormMedia->setMaximumSize(QSize(600, 120));
        verticalLayout_2 = new QVBoxLayout(FormMedia);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        stackedWidget = new QStackedWidget(FormMedia);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setStyleSheet(QString::fromUtf8(""));
        addRadio = new QWidget();
        addRadio->setObjectName(QString::fromUtf8("addRadio"));
        verticalLayout_3 = new QVBoxLayout(addRadio);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QString::fromUtf8("gridLayout_2"));
        editAddTitre = new QLineEdit(addRadio);
        editAddTitre->setObjectName(QString::fromUtf8("editAddTitre"));
        editAddTitre->setStyleSheet(QString::fromUtf8(""));

        gridLayout_2->addWidget(editAddTitre, 0, 1, 1, 2);

        label_3 = new QLabel(addRadio);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setStyleSheet(QString::fromUtf8("font: 75 11pt \"Comfortaa\";\n"
"border:1px solid rgb(3, 34, 76);\n"
"padding:3px;\n"
"color:black;\n"
"text-align:center;\n"
"letter-spacing:1px;"));

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label_2 = new QLabel(addRadio);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setStyleSheet(QString::fromUtf8("font: 75 11pt \"Comfortaa\";\n"
"border:1px solid rgb(3, 34, 76);\n"
"padding:3px;\n"
"color:black;\n"
"text-align:center;\n"
"letter-spacing:1px;"));

        horizontalLayout->addWidget(label_2);

        editUrl = new QLineEdit(addRadio);
        editUrl->setObjectName(QString::fromUtf8("editUrl"));
        editUrl->setStyleSheet(QString::fromUtf8(""));

        horizontalLayout->addWidget(editUrl);


        verticalLayout_3->addLayout(horizontalLayout);

        stackedWidget->addWidget(addRadio);
        removeRadio = new QWidget();
        removeRadio->setObjectName(QString::fromUtf8("removeRadio"));
        verticalLayout_4 = new QVBoxLayout(removeRadio);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new QLabel(removeRadio);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        editRemoveTitre = new QLineEdit(removeRadio);
        editRemoveTitre->setObjectName(QString::fromUtf8("editRemoveTitre"));

        gridLayout->addWidget(editRemoveTitre, 0, 1, 1, 2);


        verticalLayout_4->addLayout(gridLayout);

        stackedWidget->addWidget(removeRadio);

        verticalLayout->addWidget(stackedWidget);

        buttonBox = new QDialogButtonBox(FormMedia);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;\n"
"font: 11pt \"Comfortaa\";"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(FormMedia);
        QObject::connect(buttonBox, SIGNAL(accepted()), FormMedia, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), FormMedia, SLOT(reject()));

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(FormMedia);
    } // setupUi

    void retranslateUi(QDialog *FormMedia)
    {
        FormMedia->setWindowTitle(QCoreApplication::translate("FormMedia", "Dialog", nullptr));
        label_3->setText(QCoreApplication::translate("FormMedia", "Titre du media", nullptr));
        label_2->setText(QCoreApplication::translate("FormMedia", "Url du media", nullptr));
        label->setText(QCoreApplication::translate("FormMedia", "Titre Station", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FormMedia: public Ui_FormMedia {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMRADIO_H
