#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <equalizerbar.h>
#include <formradio.h>
#include <media.h>

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QStatusBar>
#include <QStandardItem>
#include <QTextStream>
#include <QFile>
#include <QMediaPlayer>
#include <QMediaPlaylist>
#include <QDirIterator>
#include <QList>
#include <QFileDialog>
#include <QCoreApplication>
#include <QFileIconProvider>

#include <QtNetwork/QNetworkReply>
#include <iostream>
#include <random>

enum class Mode { Enligne, Local };

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void selectMedia();
    void addMedia(Media media);
    void newRow(Media media);
    void viderTable();
private slots:
    // Action sur le timer
    void on_timerUpdate();

    // Action sur les différents modes
    void on_actionEnligne_triggered();
    void on_actionLocal_triggered();

    // Action sur le fichier
    void on_actionOuvrir_triggered();
    void on_actionSauvegarder_triggered();
    void on_actionQuitter_triggered();

    // Action sur la playlist
    void on_actionAjouter_Radio_triggered();
    void on_actionSupprimer_Radio_triggered();
    void on_actionVider_triggered();
    void on_actionNext_clicked();
    void on_actionPrevious_clicked();

    // Affiche les listes des favories et les filtres
    void on_tableWidget_cellClicked(int row, int column);
    void on_actionFavoris_clicked();
    void on_editRecherche_editingFinished();

    // Action sur le média
    void on_actionPlay_clicked();
    void on_actionPause_clicked();
    void on_actionStop_clicked();
    void on_actionMute_toggled(bool checked);
    void on_actionVolume_valueChanged(int value);

    void error(QNetworkReply::NetworkError code);
private:
    Ui::MainWindow *ui;
    FormMedia *formMedia;

    QMediaPlaylist *playList;
    QMediaPlayer *player;
    EqualizerBar *bar;
    QTimer *_timer;

    QString dirPath;
    QIcon iconPlay, iconPause, iconStop;
    QIcon iconPrevious, iconNext;
    QIcon iconSon, iconMute;
    QIcon iconFav, iconNot_Fav;
    QIcon iconMusic, iconRadio;

    QList<Media> list;
    QList<int> listActive;
    Mode mode;

    bool listeFavorite = false;
    bool listeFiltrer = false;
    unsigned int nbBars = 22;
    unsigned int nbSteps = 16;
};

#endif // MAINWINDOW_H
