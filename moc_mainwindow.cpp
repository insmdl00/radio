/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.15.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include <memory>
#include "mainwindow.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.15.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_MainWindow_t {
    QByteArrayData data[28];
    char stringdata0[571];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_MainWindow_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_MainWindow_t qt_meta_stringdata_MainWindow = {
    {
QT_MOC_LITERAL(0, 0, 10), // "MainWindow"
QT_MOC_LITERAL(1, 11, 14), // "on_timerUpdate"
QT_MOC_LITERAL(2, 26, 0), // ""
QT_MOC_LITERAL(3, 27, 26), // "on_actionEnligne_triggered"
QT_MOC_LITERAL(4, 54, 24), // "on_actionLocal_triggered"
QT_MOC_LITERAL(5, 79, 25), // "on_actionOuvrir_triggered"
QT_MOC_LITERAL(6, 105, 30), // "on_actionSauvegarder_triggered"
QT_MOC_LITERAL(7, 136, 26), // "on_actionQuitter_triggered"
QT_MOC_LITERAL(8, 163, 32), // "on_actionAjouter_Radio_triggered"
QT_MOC_LITERAL(9, 196, 34), // "on_actionSupprimer_Radio_trig..."
QT_MOC_LITERAL(10, 231, 24), // "on_actionVider_triggered"
QT_MOC_LITERAL(11, 256, 21), // "on_actionNext_clicked"
QT_MOC_LITERAL(12, 278, 25), // "on_actionPrevious_clicked"
QT_MOC_LITERAL(13, 304, 26), // "on_tableWidget_cellClicked"
QT_MOC_LITERAL(14, 331, 3), // "row"
QT_MOC_LITERAL(15, 335, 6), // "column"
QT_MOC_LITERAL(16, 342, 24), // "on_actionFavoris_clicked"
QT_MOC_LITERAL(17, 367, 32), // "on_editRecherche_editingFinished"
QT_MOC_LITERAL(18, 400, 21), // "on_actionPlay_clicked"
QT_MOC_LITERAL(19, 422, 22), // "on_actionPause_clicked"
QT_MOC_LITERAL(20, 445, 21), // "on_actionStop_clicked"
QT_MOC_LITERAL(21, 467, 21), // "on_actionMute_toggled"
QT_MOC_LITERAL(22, 489, 7), // "checked"
QT_MOC_LITERAL(23, 497, 28), // "on_actionVolume_valueChanged"
QT_MOC_LITERAL(24, 526, 5), // "value"
QT_MOC_LITERAL(25, 532, 5), // "error"
QT_MOC_LITERAL(26, 538, 27), // "QNetworkReply::NetworkError"
QT_MOC_LITERAL(27, 566, 4) // "code"

    },
    "MainWindow\0on_timerUpdate\0\0"
    "on_actionEnligne_triggered\0"
    "on_actionLocal_triggered\0"
    "on_actionOuvrir_triggered\0"
    "on_actionSauvegarder_triggered\0"
    "on_actionQuitter_triggered\0"
    "on_actionAjouter_Radio_triggered\0"
    "on_actionSupprimer_Radio_triggered\0"
    "on_actionVider_triggered\0on_actionNext_clicked\0"
    "on_actionPrevious_clicked\0"
    "on_tableWidget_cellClicked\0row\0column\0"
    "on_actionFavoris_clicked\0"
    "on_editRecherche_editingFinished\0"
    "on_actionPlay_clicked\0on_actionPause_clicked\0"
    "on_actionStop_clicked\0on_actionMute_toggled\0"
    "checked\0on_actionVolume_valueChanged\0"
    "value\0error\0QNetworkReply::NetworkError\0"
    "code"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_MainWindow[] = {

 // content:
       8,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,  114,    2, 0x08 /* Private */,
       3,    0,  115,    2, 0x08 /* Private */,
       4,    0,  116,    2, 0x08 /* Private */,
       5,    0,  117,    2, 0x08 /* Private */,
       6,    0,  118,    2, 0x08 /* Private */,
       7,    0,  119,    2, 0x08 /* Private */,
       8,    0,  120,    2, 0x08 /* Private */,
       9,    0,  121,    2, 0x08 /* Private */,
      10,    0,  122,    2, 0x08 /* Private */,
      11,    0,  123,    2, 0x08 /* Private */,
      12,    0,  124,    2, 0x08 /* Private */,
      13,    2,  125,    2, 0x08 /* Private */,
      16,    0,  130,    2, 0x08 /* Private */,
      17,    0,  131,    2, 0x08 /* Private */,
      18,    0,  132,    2, 0x08 /* Private */,
      19,    0,  133,    2, 0x08 /* Private */,
      20,    0,  134,    2, 0x08 /* Private */,
      21,    1,  135,    2, 0x08 /* Private */,
      23,    1,  138,    2, 0x08 /* Private */,
      25,    1,  141,    2, 0x08 /* Private */,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int, QMetaType::Int,   14,   15,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Bool,   22,
    QMetaType::Void, QMetaType::Int,   24,
    QMetaType::Void, 0x80000000 | 26,   27,

       0        // eod
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        auto *_t = static_cast<MainWindow *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->on_timerUpdate(); break;
        case 1: _t->on_actionEnligne_triggered(); break;
        case 2: _t->on_actionLocal_triggered(); break;
        case 3: _t->on_actionOuvrir_triggered(); break;
        case 4: _t->on_actionSauvegarder_triggered(); break;
        case 5: _t->on_actionQuitter_triggered(); break;
        case 6: _t->on_actionAjouter_Radio_triggered(); break;
        case 7: _t->on_actionSupprimer_Radio_triggered(); break;
        case 8: _t->on_actionVider_triggered(); break;
        case 9: _t->on_actionNext_clicked(); break;
        case 10: _t->on_actionPrevious_clicked(); break;
        case 11: _t->on_tableWidget_cellClicked((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 12: _t->on_actionFavoris_clicked(); break;
        case 13: _t->on_editRecherche_editingFinished(); break;
        case 14: _t->on_actionPlay_clicked(); break;
        case 15: _t->on_actionPause_clicked(); break;
        case 16: _t->on_actionStop_clicked(); break;
        case 17: _t->on_actionMute_toggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 18: _t->on_actionVolume_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 19: _t->error((*reinterpret_cast< QNetworkReply::NetworkError(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 19:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QNetworkReply::NetworkError >(); break;
            }
            break;
        }
    }
}

QT_INIT_METAOBJECT const QMetaObject MainWindow::staticMetaObject = { {
    QMetaObject::SuperData::link<QMainWindow::staticMetaObject>(),
    qt_meta_stringdata_MainWindow.data,
    qt_meta_data_MainWindow,
    qt_static_metacall,
    nullptr,
    nullptr
} };


const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
