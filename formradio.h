#ifndef FORMRADIO_H
#define FORMRADIO_H

#include <QDialog>

namespace Ui {
class FormMedia;
}

class FormMedia : public QDialog
{
    Q_OBJECT

public:
    explicit FormMedia(QWidget *parent = nullptr);
    ~FormMedia();

    const QString getTitre() const;
    const QString getUrl() const;
    void resetForm();
    void setPage(int i);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();

private:
    Ui::FormMedia *ui;
};

#endif // FORMRADIO_H
