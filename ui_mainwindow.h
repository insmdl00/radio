/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.15.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOuvrir;
    QAction *actionQuitter;
    QAction *actionAjouter_Radio;
    QAction *actionSupprimer_Radio;
    QAction *actionSauvegarder;
    QAction *actionVider;
    QAction *actionEnligne;
    QAction *actionLocal;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *editRecherche;
    QPushButton *actionFavoris;
    QHBoxLayout *horizontalLayout_4;
    QTableWidget *tableWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *actionPlay;
    QPushButton *actionPause;
    QPushButton *actionStop;
    QFrame *line;
    QPushButton *actionPrevious;
    QPushButton *actionNext;
    QHBoxLayout *horizontalLayout_5;
    QSlider *progressBar;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *actionMute;
    QSlider *actionVolume;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QMenu *menuRadio;
    QMenu *menuMode;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1032, 429);
        MainWindow->setStyleSheet(QString::fromUtf8(""));
        actionOuvrir = new QAction(MainWindow);
        actionOuvrir->setObjectName(QString::fromUtf8("actionOuvrir"));
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QString::fromUtf8("actionQuitter"));
        actionAjouter_Radio = new QAction(MainWindow);
        actionAjouter_Radio->setObjectName(QString::fromUtf8("actionAjouter_Radio"));
        actionAjouter_Radio->setVisible(true);
        actionSupprimer_Radio = new QAction(MainWindow);
        actionSupprimer_Radio->setObjectName(QString::fromUtf8("actionSupprimer_Radio"));
        actionSauvegarder = new QAction(MainWindow);
        actionSauvegarder->setObjectName(QString::fromUtf8("actionSauvegarder"));
        actionVider = new QAction(MainWindow);
        actionVider->setObjectName(QString::fromUtf8("actionVider"));
        actionEnligne = new QAction(MainWindow);
        actionEnligne->setObjectName(QString::fromUtf8("actionEnligne"));
        actionLocal = new QAction(MainWindow);
        actionLocal->setObjectName(QString::fromUtf8("actionLocal"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        centralWidget->setAutoFillBackground(false);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("font: 75 11pt \"Comfortaa\";\n"
"border:1px solid rgb(3, 34, 76);\n"
"padding:3px;\n"
"color:black;\n"
"text-align:center;\n"
"letter-spacing:1px;"));

        horizontalLayout_3->addWidget(label);

        editRecherche = new QLineEdit(centralWidget);
        editRecherche->setObjectName(QString::fromUtf8("editRecherche"));

        horizontalLayout_3->addWidget(editRecherche);

        actionFavoris = new QPushButton(centralWidget);
        actionFavoris->setObjectName(QString::fromUtf8("actionFavoris"));
        actionFavoris->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;\n"
"font: 11pt \"Comfortaa\";\n"
""));

        horizontalLayout_3->addWidget(actionFavoris);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 3)
            tableWidget->setColumnCount(3);
        QIcon icon;
        icon.addFile(QString::fromUtf8("../Icones/not_fav.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setIcon(icon);
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setTextAlignment(Qt::AlignCenter);
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setMaximumSize(QSize(320, 16777215));
        QFont font;
        font.setFamily(QString::fromUtf8("Comfortaa"));
        font.setPointSize(11);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        tableWidget->setFont(font);
        tableWidget->setStyleSheet(QString::fromUtf8("font: 11pt \"Comfortaa\";\n"
""));
        tableWidget->setIconSize(QSize(20, 20));
        tableWidget->setTextElideMode(Qt::ElideMiddle);
        tableWidget->setShowGrid(false);
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setMinimumSectionSize(25);
        tableWidget->horizontalHeader()->setDefaultSectionSize(25);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setMinimumSectionSize(25);
        tableWidget->verticalHeader()->setDefaultSectionSize(25);
        tableWidget->verticalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->verticalHeader()->setStretchLastSection(false);

        horizontalLayout_4->addWidget(tableWidget);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));

        horizontalLayout_4->addLayout(verticalLayout_3);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        actionPlay = new QPushButton(centralWidget);
        actionPlay->setObjectName(QString::fromUtf8("actionPlay"));
        actionPlay->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;\n"
"\n"
""));

        horizontalLayout->addWidget(actionPlay);

        actionPause = new QPushButton(centralWidget);
        actionPause->setObjectName(QString::fromUtf8("actionPause"));
        actionPause->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;\n"
"\n"
""));

        horizontalLayout->addWidget(actionPause);

        actionStop = new QPushButton(centralWidget);
        actionStop->setObjectName(QString::fromUtf8("actionStop"));
        actionStop->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("../Icones/stop-button.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStop->setIcon(icon1);

        horizontalLayout->addWidget(actionStop);

        line = new QFrame(centralWidget);
        line->setObjectName(QString::fromUtf8("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);

        actionPrevious = new QPushButton(centralWidget);
        actionPrevious->setObjectName(QString::fromUtf8("actionPrevious"));
        actionPrevious->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;"));

        horizontalLayout->addWidget(actionPrevious);

        actionNext = new QPushButton(centralWidget);
        actionNext->setObjectName(QString::fromUtf8("actionNext"));
        actionNext->setEnabled(true);
        actionNext->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;"));

        horizontalLayout->addWidget(actionNext);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        progressBar = new QSlider(centralWidget);
        progressBar->setObjectName(QString::fromUtf8("progressBar"));
        progressBar->setEnabled(false);
        progressBar->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(progressBar);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        actionMute = new QPushButton(centralWidget);
        actionMute->setObjectName(QString::fromUtf8("actionMute"));
        QFont font1;
        font1.setBold(false);
        font1.setWeight(50);
        actionMute->setFont(font1);
        actionMute->setStyleSheet(QString::fromUtf8("background-color:#bbd2e1;"));
        actionMute->setCheckable(true);

        horizontalLayout_2->addWidget(actionMute);

        actionVolume = new QSlider(centralWidget);
        actionVolume->setObjectName(QString::fromUtf8("actionVolume"));
        actionVolume->setValue(50);
        actionVolume->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(actionVolume);


        verticalLayout->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1032, 23));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QString::fromUtf8("menuFichier"));
        menuRadio = new QMenu(menuBar);
        menuRadio->setObjectName(QString::fromUtf8("menuRadio"));
        menuMode = new QMenu(menuBar);
        menuMode->setObjectName(QString::fromUtf8("menuMode"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuBar->addAction(menuMode->menuAction());
        menuBar->addAction(menuRadio->menuAction());
        menuFichier->addAction(actionOuvrir);
        menuFichier->addAction(actionSauvegarder);
        menuFichier->addSeparator();
        menuFichier->addAction(actionQuitter);
        menuRadio->addAction(actionAjouter_Radio);
        menuRadio->addAction(actionSupprimer_Radio);
        menuRadio->addSeparator();
        menuRadio->addAction(actionVider);
        menuMode->addAction(actionEnligne);
        menuMode->addAction(actionLocal);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        actionOuvrir->setText(QCoreApplication::translate("MainWindow", "Ouvrir", nullptr));
#if QT_CONFIG(shortcut)
        actionOuvrir->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_CONFIG(shortcut)
        actionQuitter->setText(QCoreApplication::translate("MainWindow", "Quitter", nullptr));
#if QT_CONFIG(shortcut)
        actionQuitter->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_CONFIG(shortcut)
        actionAjouter_Radio->setText(QCoreApplication::translate("MainWindow", "Ajouter Media", nullptr));
        actionSupprimer_Radio->setText(QCoreApplication::translate("MainWindow", "Supprimer Media", nullptr));
        actionSauvegarder->setText(QCoreApplication::translate("MainWindow", "Sauvegarder", nullptr));
#if QT_CONFIG(shortcut)
        actionSauvegarder->setShortcut(QCoreApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_CONFIG(shortcut)
        actionVider->setText(QCoreApplication::translate("MainWindow", "Vider", nullptr));
        actionEnligne->setText(QCoreApplication::translate("MainWindow", "Enligne", nullptr));
        actionLocal->setText(QCoreApplication::translate("MainWindow", "Local", nullptr));
        label->setText(QCoreApplication::translate("MainWindow", "Recherche : ", nullptr));
        actionFavoris->setText(QCoreApplication::translate("MainWindow", "Favoris", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem->setText(QCoreApplication::translate("MainWindow", "Nom du Media", nullptr));
        actionPlay->setText(QString());
        actionPause->setText(QString());
        actionStop->setText(QString());
        actionPrevious->setText(QString());
        actionNext->setText(QString());
        actionMute->setText(QString());
        menuFichier->setTitle(QCoreApplication::translate("MainWindow", "Fichier", nullptr));
        menuRadio->setTitle(QCoreApplication::translate("MainWindow", "Media", nullptr));
        menuMode->setTitle(QCoreApplication::translate("MainWindow", "Mode", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
