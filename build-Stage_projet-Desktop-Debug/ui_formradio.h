/********************************************************************************
** Form generated from reading UI file 'formradio.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORMRADIO_H
#define UI_FORMRADIO_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_FormMedia
{
public:
    QVBoxLayout *verticalLayout_2;
    QVBoxLayout *verticalLayout;
    QStackedWidget *stackedWidget;
    QWidget *addRadio;
    QVBoxLayout *verticalLayout_3;
    QGridLayout *gridLayout_2;
    QLineEdit *editAddTitre;
    QLabel *label_3;
    QHBoxLayout *horizontalLayout;
    QLabel *label_2;
    QLineEdit *editUrl;
    QWidget *removeRadio;
    QVBoxLayout *verticalLayout_4;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *editRemoveTitre;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *FormMedia)
    {
        if (FormMedia->objectName().isEmpty())
            FormMedia->setObjectName(QStringLiteral("FormMedia"));
        FormMedia->resize(600, 120);
        FormMedia->setMaximumSize(QSize(600, 120));
        verticalLayout_2 = new QVBoxLayout(FormMedia);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        stackedWidget = new QStackedWidget(FormMedia);
        stackedWidget->setObjectName(QStringLiteral("stackedWidget"));
        addRadio = new QWidget();
        addRadio->setObjectName(QStringLiteral("addRadio"));
        verticalLayout_3 = new QVBoxLayout(addRadio);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        editAddTitre = new QLineEdit(addRadio);
        editAddTitre->setObjectName(QStringLiteral("editAddTitre"));

        gridLayout_2->addWidget(editAddTitre, 0, 1, 1, 2);

        label_3 = new QLabel(addRadio);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout_2->addWidget(label_3, 0, 0, 1, 1);


        verticalLayout_3->addLayout(gridLayout_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_2 = new QLabel(addRadio);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout->addWidget(label_2);

        editUrl = new QLineEdit(addRadio);
        editUrl->setObjectName(QStringLiteral("editUrl"));

        horizontalLayout->addWidget(editUrl);


        verticalLayout_3->addLayout(horizontalLayout);

        stackedWidget->addWidget(addRadio);
        removeRadio = new QWidget();
        removeRadio->setObjectName(QStringLiteral("removeRadio"));
        verticalLayout_4 = new QVBoxLayout(removeRadio);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(removeRadio);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        editRemoveTitre = new QLineEdit(removeRadio);
        editRemoveTitre->setObjectName(QStringLiteral("editRemoveTitre"));

        gridLayout->addWidget(editRemoveTitre, 0, 1, 1, 2);


        verticalLayout_4->addLayout(gridLayout);

        stackedWidget->addWidget(removeRadio);

        verticalLayout->addWidget(stackedWidget);

        buttonBox = new QDialogButtonBox(FormMedia);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        verticalLayout_2->addLayout(verticalLayout);


        retranslateUi(FormMedia);
        QObject::connect(buttonBox, SIGNAL(accepted()), FormMedia, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), FormMedia, SLOT(reject()));

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(FormMedia);
    } // setupUi

    void retranslateUi(QDialog *FormMedia)
    {
        FormMedia->setWindowTitle(QApplication::translate("FormMedia", "Dialog", nullptr));
        label_3->setText(QApplication::translate("FormMedia", "Titre du media", nullptr));
        label_2->setText(QApplication::translate("FormMedia", "Url du media", nullptr));
        label->setText(QApplication::translate("FormMedia", "Titre Station", nullptr));
    } // retranslateUi

};

namespace Ui {
    class FormMedia: public Ui_FormMedia {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORMRADIO_H
