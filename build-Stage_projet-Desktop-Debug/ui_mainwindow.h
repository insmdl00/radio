/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.11.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOuvrir;
    QAction *actionQuitter;
    QAction *actionAjouter_Radio;
    QAction *actionSupprimer_Radio;
    QAction *actionSauvegarder;
    QAction *actionVider;
    QAction *actionEnligne;
    QAction *actionLocal;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLineEdit *editRecherche;
    QPushButton *actionFavoris;
    QHBoxLayout *horizontalLayout_4;
    QTableWidget *tableWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *actionPlay;
    QPushButton *actionPause;
    QPushButton *actionStop;
    QFrame *line;
    QPushButton *actionPrevious;
    QPushButton *actionNext;
    QHBoxLayout *horizontalLayout_5;
    QSlider *progressBar;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *actionMute;
    QSlider *actionVolume;
    QMenuBar *menuBar;
    QMenu *menuFichier;
    QMenu *menuRadio;
    QMenu *menuMode;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1032, 429);
        MainWindow->setStyleSheet(QStringLiteral(""));
        actionOuvrir = new QAction(MainWindow);
        actionOuvrir->setObjectName(QStringLiteral("actionOuvrir"));
        actionQuitter = new QAction(MainWindow);
        actionQuitter->setObjectName(QStringLiteral("actionQuitter"));
        actionAjouter_Radio = new QAction(MainWindow);
        actionAjouter_Radio->setObjectName(QStringLiteral("actionAjouter_Radio"));
        actionAjouter_Radio->setVisible(true);
        actionSupprimer_Radio = new QAction(MainWindow);
        actionSupprimer_Radio->setObjectName(QStringLiteral("actionSupprimer_Radio"));
        actionSauvegarder = new QAction(MainWindow);
        actionSauvegarder->setObjectName(QStringLiteral("actionSauvegarder"));
        actionVider = new QAction(MainWindow);
        actionVider->setObjectName(QStringLiteral("actionVider"));
        actionEnligne = new QAction(MainWindow);
        actionEnligne->setObjectName(QStringLiteral("actionEnligne"));
        actionLocal = new QAction(MainWindow);
        actionLocal->setObjectName(QStringLiteral("actionLocal"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        centralWidget->setAutoFillBackground(false);
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setStyleSheet(QStringLiteral(""));

        horizontalLayout_3->addWidget(label);

        editRecherche = new QLineEdit(centralWidget);
        editRecherche->setObjectName(QStringLiteral("editRecherche"));

        horizontalLayout_3->addWidget(editRecherche);

        actionFavoris = new QPushButton(centralWidget);
        actionFavoris->setObjectName(QStringLiteral("actionFavoris"));

        horizontalLayout_3->addWidget(actionFavoris);


        verticalLayout->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        tableWidget = new QTableWidget(centralWidget);
        if (tableWidget->columnCount() < 2)
            tableWidget->setColumnCount(2);
        QIcon icon;
        icon.addFile(QStringLiteral("../Icones/not_fav.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setIcon(icon);
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        tableWidget->setObjectName(QStringLiteral("tableWidget"));
        tableWidget->setMaximumSize(QSize(320, 16777215));
        QFont font;
        font.setFamily(QStringLiteral("calibri,sans-serif"));
        tableWidget->setFont(font);
        tableWidget->setStyleSheet(QStringLiteral(""));
        tableWidget->horizontalHeader()->setCascadingSectionResizes(false);
        tableWidget->horizontalHeader()->setDefaultSectionSize(22);
        tableWidget->horizontalHeader()->setHighlightSections(true);
        tableWidget->horizontalHeader()->setMinimumSectionSize(5);
        tableWidget->horizontalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->horizontalHeader()->setStretchLastSection(true);
        tableWidget->verticalHeader()->setDefaultSectionSize(50);
        tableWidget->verticalHeader()->setMinimumSectionSize(50);
        tableWidget->verticalHeader()->setProperty("showSortIndicator", QVariant(false));
        tableWidget->verticalHeader()->setStretchLastSection(false);

        horizontalLayout_4->addWidget(tableWidget);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));

        horizontalLayout_4->addLayout(verticalLayout_3);


        verticalLayout->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setSizeConstraint(QLayout::SetMaximumSize);
        actionPlay = new QPushButton(centralWidget);
        actionPlay->setObjectName(QStringLiteral("actionPlay"));
        actionPlay->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(actionPlay);

        actionPause = new QPushButton(centralWidget);
        actionPause->setObjectName(QStringLiteral("actionPause"));
        actionPause->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(actionPause);

        actionStop = new QPushButton(centralWidget);
        actionStop->setObjectName(QStringLiteral("actionStop"));
        actionStop->setStyleSheet(QStringLiteral(""));
        QIcon icon1;
        icon1.addFile(QStringLiteral("../Icones/stop-button.png"), QSize(), QIcon::Normal, QIcon::Off);
        actionStop->setIcon(icon1);

        horizontalLayout->addWidget(actionStop);

        line = new QFrame(centralWidget);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);

        horizontalLayout->addWidget(line);

        actionPrevious = new QPushButton(centralWidget);
        actionPrevious->setObjectName(QStringLiteral("actionPrevious"));
        actionPrevious->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(actionPrevious);

        actionNext = new QPushButton(centralWidget);
        actionNext->setObjectName(QStringLiteral("actionNext"));
        actionNext->setEnabled(true);
        actionNext->setStyleSheet(QStringLiteral(""));

        horizontalLayout->addWidget(actionNext);


        verticalLayout->addLayout(horizontalLayout);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        progressBar = new QSlider(centralWidget);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setEnabled(false);
        progressBar->setOrientation(Qt::Horizontal);

        horizontalLayout_5->addWidget(progressBar);


        verticalLayout->addLayout(horizontalLayout_5);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        actionMute = new QPushButton(centralWidget);
        actionMute->setObjectName(QStringLiteral("actionMute"));
        QFont font1;
        font1.setBold(false);
        font1.setWeight(50);
        actionMute->setFont(font1);
        actionMute->setStyleSheet(QStringLiteral(""));
        actionMute->setCheckable(true);

        horizontalLayout_2->addWidget(actionMute);

        actionVolume = new QSlider(centralWidget);
        actionVolume->setObjectName(QStringLiteral("actionVolume"));
        actionVolume->setValue(50);
        actionVolume->setOrientation(Qt::Horizontal);

        horizontalLayout_2->addWidget(actionVolume);


        verticalLayout->addLayout(horizontalLayout_2);

        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1032, 23));
        menuFichier = new QMenu(menuBar);
        menuFichier->setObjectName(QStringLiteral("menuFichier"));
        menuRadio = new QMenu(menuBar);
        menuRadio->setObjectName(QStringLiteral("menuRadio"));
        menuMode = new QMenu(menuBar);
        menuMode->setObjectName(QStringLiteral("menuMode"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFichier->menuAction());
        menuBar->addAction(menuMode->menuAction());
        menuBar->addAction(menuRadio->menuAction());
        menuFichier->addAction(actionOuvrir);
        menuFichier->addAction(actionSauvegarder);
        menuFichier->addSeparator();
        menuFichier->addAction(actionQuitter);
        menuRadio->addAction(actionAjouter_Radio);
        menuRadio->addAction(actionSupprimer_Radio);
        menuRadio->addSeparator();
        menuRadio->addAction(actionVider);
        menuMode->addAction(actionEnligne);
        menuMode->addAction(actionLocal);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        actionOuvrir->setText(QApplication::translate("MainWindow", "Ouvrir", nullptr));
#ifndef QT_NO_SHORTCUT
        actionOuvrir->setShortcut(QApplication::translate("MainWindow", "Ctrl+O", nullptr));
#endif // QT_NO_SHORTCUT
        actionQuitter->setText(QApplication::translate("MainWindow", "Quitter", nullptr));
#ifndef QT_NO_SHORTCUT
        actionQuitter->setShortcut(QApplication::translate("MainWindow", "Ctrl+Q", nullptr));
#endif // QT_NO_SHORTCUT
        actionAjouter_Radio->setText(QApplication::translate("MainWindow", "Ajouter Media", nullptr));
        actionSupprimer_Radio->setText(QApplication::translate("MainWindow", "Supprimer Media", nullptr));
        actionSauvegarder->setText(QApplication::translate("MainWindow", "Sauvegarder", nullptr));
#ifndef QT_NO_SHORTCUT
        actionSauvegarder->setShortcut(QApplication::translate("MainWindow", "Ctrl+S", nullptr));
#endif // QT_NO_SHORTCUT
        actionVider->setText(QApplication::translate("MainWindow", "Vider", nullptr));
        actionEnligne->setText(QApplication::translate("MainWindow", "Enligne", nullptr));
        actionLocal->setText(QApplication::translate("MainWindow", "Local", nullptr));
        label->setText(QApplication::translate("MainWindow", "Recherche : ", nullptr));
        actionFavoris->setText(QApplication::translate("MainWindow", "Favoris", nullptr));
        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem->setText(QApplication::translate("MainWindow", "Nom du Media", nullptr));
        actionPlay->setText(QString());
        actionPause->setText(QString());
        actionStop->setText(QString());
        actionPrevious->setText(QString());
        actionNext->setText(QString());
        actionMute->setText(QString());
        menuFichier->setTitle(QApplication::translate("MainWindow", "Fichier", nullptr));
        menuRadio->setTitle(QApplication::translate("MainWindow", "Media", nullptr));
        menuMode->setTitle(QApplication::translate("MainWindow", "Mode", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
