#include "formradio.h"
#include "ui_formradio.h"

FormMedia::FormMedia(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FormMedia) {
    ui->setupUi(this);
}

FormMedia::~FormMedia() {
    delete ui;
}

void FormMedia::resetForm() {
    ui->editAddTitre->setText("");
    ui->editRemoveTitre->setText("");
    ui->editUrl->setText("");
}

void FormMedia::setPage(int i) {
    resetForm();
    ui->stackedWidget->setCurrentIndex(i);
    if(i == 0) setWindowTitle("Ajouter une Media");
    else if(i == 1) setWindowTitle("Supprimer une Media");
}

const QString FormMedia::getTitre() const {
    if(ui->stackedWidget->currentIndex() == 0) return ui->editAddTitre->text();
    else if(ui->stackedWidget->currentIndex() == 1) return ui->editRemoveTitre->text();
    else return nullptr;
}

const QString FormMedia::getUrl() const {
    return ui->editUrl->text();
}

void FormMedia::on_buttonBox_accepted() {
    accept();
}

void FormMedia::on_buttonBox_rejected() {
    reject();
}
