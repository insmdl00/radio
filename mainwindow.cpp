#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkReply>
#include <QQuickImageProvider>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent),
    ui(new Ui::MainWindow), formMedia(new FormMedia(this)) {
    ui->setupUi(this);

    dirPath = QCoreApplication::applicationDirPath();
    iconPlay = QIcon(dirPath + "/Icones/play.png");
    iconPause = QIcon(dirPath + "/Icones/pause.png");
    iconStop = QIcon(dirPath + "/Icones/stop.png");
    iconPrevious = QIcon(dirPath + "/Icones/previous.png");
    iconNext = QIcon(dirPath + "/Icones/next.png");
    iconSon = QIcon(dirPath + "/Icones/son.png");
    iconMute = QIcon(dirPath + "/Icones/mute.png");
    iconFav = QIcon(dirPath + "/Icones/fav.png");
    iconNot_Fav = QIcon(dirPath + "/Icones/not_fav.png");
    iconMusic = QIcon(dirPath + "/Icones/music.png");
    iconRadio = QIcon(dirPath + "/Icones/radio.png");

    playList = new QMediaPlaylist(this);

    player = new QMediaPlayer(this);
    bar = new EqualizerBar(this);

    player->setPlaylist(playList);
    player->setVolume(ui->actionVolume->value());

    bar->setGradient(QColor("#ff7070"), QColor("#0099cc"), nbSteps);
    ui->actionPlay->setIcon(iconPlay);
    ui->actionPause->setIcon(iconPause);
    ui->actionStop->setIcon(iconStop);
    ui->actionPrevious->setIcon(iconPrevious);
    ui->actionNext->setIcon(iconNext);
    ui->actionMute->setIcon(iconSon);
    ui->verticalLayout_3->addWidget(bar);

    _timer = new QTimer();
    _timer->setInterval(100);
    connect(_timer,SIGNAL(timeout()),this,SLOT(on_timerUpdate()));
    _timer->start();

    connect(player, &QMediaPlayer::durationChanged, [&](int dur) { ui->progressBar->setMaximum(dur); });
    connect(player, &QMediaPlayer::positionChanged, [&](int pos) { ui->progressBar->setValue(pos);  });

    on_actionLocal_triggered();
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::selectMedia() {
    int i = playList->currentIndex();
    if(i >= 0) {
        int id = listActive.at(i);
        Media media = list.at(id);

        std::cout << "Url du media en cours : " << media.toStringUrl() << std::endl;
        if(mode == Mode::Local) setWindowTitle("PlayList Local : " + media.getTitre());
        else setWindowTitle("PlayList Enligne : " + media.getTitre());
    } else {
        if(mode == Mode::Local) setWindowTitle("PlayList Local");
        else setWindowTitle("PlayList Enligne");
    }
}

void MainWindow::addMedia(Media media) {
    if(media.valideMedia() and media.getTitre() != "") {
        list.append(media);
        if(!listeFavorite and !listeFiltrer) {
            listActive.append(list.size() - 1);
            newRow(media);
        }
    } else std::cout << "L'url : " << media.toStringUrl() << " est non valide !" << std::endl;
}

void MainWindow::newRow(Media media) {
    int row = ui->tableWidget->rowCount();

    QTableWidgetItem *fav = new QTableWidgetItem;
    fav -> setTextAlignment(Qt::AlignCenter);
    if(media.getFavori()) fav->setIcon(iconFav);
    else fav->setIcon(iconNot_Fav);

    fav->setSizeHint(QSize(20, 20));

    QTableWidgetItem *icon = new QTableWidgetItem;
    icon->setIcon(media.getIcon());
    icon->setSizeHint(QSize(40, 40));
	
    icon -> setTextAlignment(Qt::AlignCenter);
    QTableWidgetItem *titre = new QTableWidgetItem(media.getTitre());
    titre -> setTextAlignment(Qt::AlignCenter);

    ui->tableWidget->insertRow(row);
    ui->tableWidget->setItem(row, 0, fav);
    ui->tableWidget->setItem(row, 1, icon);
    ui->tableWidget->setItem(row, 2, titre);
    playList->addMedia(media.getMedia());
}

void MainWindow::viderTable() {
    ui->tableWidget->setRowCount(0);
    playList->clear();
    listActive.clear();
}

// Action sur le timer
void MainWindow::on_timerUpdate() {
    if(player->state() == QMediaPlayer::PlayingState ) {
        std::vector<float> val;
        for(unsigned int i = 0; i < nbBars; i++) {
            val.push_back((rand() % 80) + 20);
        }
        bar->setValues(val);
    } else if(player->state() != QMediaPlayer::PausedState){
        std::vector<float> val;
        for(unsigned int i = 0; i < nbBars; i++) {
            val.push_back(20);
        }
        bar->setValues(val);
    }
}

// Action sur les fichiers
void MainWindow::on_actionOuvrir_triggered() {
    auto filename = QFileDialog::getOpenFileName(this, tr("Load a file ..."), QDir::homePath(), tr("Text File (*.txt)"));
    if (filename.isEmpty()) return;
    QFile file(filename);
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    on_actionVider_triggered();

    QTextStream xin(&file);
    xin.readLine();
    std::cout << "lecture du fichier !" << std::endl;
    while (!xin.atEnd()) {
        auto line = xin.readLine();
        QString titre = line.split(",").at(1);
        QIcon icon;

        if(line.split(",").size() == 3) {
            QUrl iconUrl =  QUrl(line.split(",").at(2));

            QNetworkAccessManager manager(this);
            QEventLoop loop;
            QNetworkReply *reply = manager.get(QNetworkRequest(iconUrl));
            MainWindow::connect(&manager,&QNetworkAccessManager::finished,&loop,&QEventLoop::quit);
            loop.exec();

            if (reply->error() != QNetworkReply::NoError) {
                qDebug() << "Network error: " << reply->error();
                qDebug() << "Url: " << line.split(",").at(2);
                icon = iconRadio;
            } else   {
                QPixmap pm;
                pm.loadFromData(reply->readAll());
                icon = QIcon(pm);
            }
            delete reply;
        } else icon = iconRadio;
        QString url = xin.readLine();

        OnlineMedia onlineMedia(url, titre, icon);
        addMedia(onlineMedia);
    }

    std::cout << "Un fichier vient d'etre lu, url : " << filename.toStdString() << std::endl;
    file.close();
}

 void MainWindow::error(QNetworkReply::NetworkError code) {
        qDebug() << "QNetworkReply::NetworkError " << code << "received";
    }

void MainWindow::on_actionSauvegarder_triggered() {
    QString filename = QFileDialog::getSaveFileName(this, tr("Save File"), QDir::homePath(), tr("Text File (*.txt)"));
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;

    // Création d'un objet QTextStream à partir de notre objet QFile
    QTextStream flux(&file);
    flux.setCodec("UTF-8");

    // Écriture des différentes lignes dans le fichier
    flux << "#EXTM3U" << endl;
    for(int i = 0; i < list.size(); i++) {
        Media media = list.at(i);
        flux << "#EXTINT:0," << media.getTitre() << endl;
        flux << media.getUrl() << endl;
    }
    std::cout << "Un fichier vient d'etre sauvegarder, url : " << filename.toStdString() << std::endl;
}

void MainWindow::on_actionQuitter_triggered(){
    this->setStatusTip(tr("Close the application"));
    QApplication::quit();
}

// Action sur les différents modes
void MainWindow::on_actionEnligne_triggered() {
    setWindowTitle("PlayList Enligne");
    on_actionVider_triggered();

    ui->actionOuvrir->setVisible(true);
    ui->actionSauvegarder->setVisible(true);
    ui->menuRadio->setDisabled(false);

    mode = Mode::Enligne;
    QFile file(QCoreApplication::applicationDirPath() + "/listeMedia.txt");
    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;

    QTextStream xin(&file);
    xin.readLine();
    while (!xin.atEnd()) {
        auto line = xin.readLine();
        QString titre = line.split(",").at(1);
        QString url = xin.readLine();

        OnlineMedia onlineMedia(url, titre, iconRadio);
        addMedia(onlineMedia);
    }
    file.close();
}

void MainWindow::on_actionLocal_triggered() {
    setWindowTitle("PlayList Local");
    on_actionVider_triggered();

    ui->actionOuvrir->setVisible(false);
    ui->actionSauvegarder->setVisible(false);
    ui->menuRadio->setDisabled(true);

    mode = Mode::Local;
    QDir path = QDir::homePath() + "/Musique";
    QFileInfoList listeFichiers = path.entryInfoList();
    for(int i = 2; i < listeFichiers.size(); i++) {
        if(listeFichiers.at(i).suffix() == "mp3" or listeFichiers.at(i).suffix() == "wav") {
            QString titre = listeFichiers.at(i).fileName().split(".").at(0);
            QString url = listeFichiers.at(i).filePath();


            LocalMedia localMedia(url, titre, iconMusic);
            list.append(localMedia);
            if(!listeFavorite and !listeFiltrer) {
                listActive.append(list.size() - 1);
                newRow(localMedia);
            }
        }
    }
}

// Action sur la playlist
void MainWindow::on_actionAjouter_Radio_triggered() {
    formMedia->setPage(0);
    int res = formMedia->exec();
    if(res == QDialog::Rejected) return;

    QString titre = formMedia->getTitre();
    QString url = formMedia->getUrl();

    OnlineMedia onlineMedia(url, titre, iconRadio);
    addMedia(onlineMedia);
}

void MainWindow::on_actionSupprimer_Radio_triggered() {
    formMedia->setPage(1);
    int res = formMedia->exec();
    if(res == QDialog::Rejected) return;

    QString text = formMedia->getTitre();
    for(int i = 0; i < list.size(); i++) {
        Media media = list.at(i);
        if(media.getTitre().contains(text, Qt::CaseInsensitive)) {
            list.removeAt(i);
            if(!listeFavorite and !listeFiltrer) {
                ui->tableWidget->removeRow(i);
                playList->removeMedia(i+1);
            }

            if(playList->currentIndex() == i) selectMedia();
            return;
        }
    }
    std::cout << "Aucune Station de Radio ne correspond à ce nom !" << std::endl;
}

void MainWindow::on_actionVider_triggered() {
    viderTable();
    list.clear();
}

void MainWindow::on_actionNext_clicked() {
    playList->next();
    player->play();
    if(playList->currentIndex() == -1)playList->next();
    selectMedia();
}

void MainWindow::on_actionPrevious_clicked() {
    playList->previous();
    player->play();
    if(playList->currentIndex() == -1)playList->previous();
    selectMedia();
}

// Affiche la liste des favoris et les filtres
void MainWindow::on_tableWidget_cellClicked(int row, int column) {
    if(column == 0) {
        QTableWidgetItem *icon_item = new QTableWidgetItem;
        int id = listActive.at(row);
        Media media = list.at(id);

        if(media.getFavori()) {
            media.setFavori(false);
            icon_item->setIcon(iconNot_Fav);
        } else {
            media.setFavori(true);
            icon_item->setIcon(iconFav);
        }

        list.replace(id, media);
        ui->tableWidget->setItem(row, 0, icon_item);
    } else if(playList->currentIndex() != row){
        playList->setCurrentIndex(row);
        player->play();
        selectMedia();
    }
}

void MainWindow::on_actionFavoris_clicked() {
    ui->editRecherche->setText("");
    listeFiltrer = false;

    listeFavorite = !listeFavorite;
    if(listeFavorite) ui->actionFavoris->setText("Non favoris");
    else ui->actionFavoris->setText("Favoris");
    viderTable();

    for(int i = 0; i < list.size(); i++) {
        Media media = list.at(i);
        if(media.getFavori() and listeFavorite) {
            listActive.append(i);
            newRow(media);
        } else if(!listeFavorite) {
            listActive.append(i);
            newRow(media);
        }
    }
    selectMedia();
}

void MainWindow::on_editRecherche_editingFinished() {
    listeFavorite = false;

    QString text = ui->editRecherche->text();
    if(text == "") listeFiltrer = false;
    else listeFiltrer = true;
    viderTable();

    for(int i = 0; i < list.size(); i++) {
        Media media = list.at(i);
        if(media.getTitre().contains(text, Qt::CaseInsensitive)) {
            listActive.append(i);
            newRow(media);
        }
    }
    selectMedia();
}

// Action sur le media
void MainWindow::on_actionPlay_clicked() {
    player->play();
    selectMedia();
}

void MainWindow::on_actionPause_clicked() {
    player->pause();
}

void MainWindow::on_actionStop_clicked() {
    player->stop();
}

void MainWindow::on_actionMute_toggled(bool checked) {
    player->setMuted(checked);

    if(checked) ui->actionMute->setIcon(iconMute);
    else ui->actionMute->setIcon(iconSon);
}

void MainWindow::on_actionVolume_valueChanged(int value) {
    player->setVolume(value);
}
